//
//  NSError+TCoreExtension.m
//  TrakCoreFramework
//
//  Created by Jose Catala on 15/11/2018.
//  Copyright © 2018 TrakGlobal Telematics. All rights reserved.
//

#import "NSError+TCoreExtension.h"

@implementation NSError (TCoreExtension)

+ (NSError *) errorWithCode:(TSDKError)code
{
    // Create localised description
    NSString *desc = NSLocalizedString([NSError descriptionFromCode:code], nil);
    
    // Create the underlying error.
    NSError *underlyingError = [[NSError alloc] initWithDomain:NSPOSIXErrorDomain code:errno userInfo:nil];
    // Create and return the custom domain error.
    NSDictionary *errorDictionary = @{NSLocalizedDescriptionKey : desc, NSUnderlyingErrorKey : underlyingError};
    
    NSError *error = [[NSError alloc] initWithDomain:@"TrakCoreErrorDomain" code:code userInfo:errorDictionary];

    return error;
}

+ (NSString *) descriptionFromCode:(TSDKError)code
{
    NSString *str = @"";
    
    switch (code) {
        case TSDKErrorUnknown:
            str = @"Unknown error";
            break;
        case TSDKErrorNotImplemented:
            str = @"Not implemented";
            break;
        case TSDKErrorDetectDeviceAlreadyActive:
            str = @"Detect device already active";
            break;
        case TSDKErrorInvalidArguments:
            str = @"Invalid arguments";
            break;
        case TSDKErrorJourneyAlreadyStarted:
            str = @"Journey already started";
            break;
        case TSDKErrorAutomaticStartConflict:
            str = @"Automatic start conflict";
            break;
        case TSDKErrorJourneyNotRecording:
            str = @"Journey not recording";
            break;
        case TSDKErrorJourneyNotFound:
            str = @"Journey not found";
            break;
        case TSDKErrorUserIdentifierNotRegistered:
            str = @"User Identifier not registered";
            break;
        case TSDKErrorApiKeyNotRegistered:
            str = @"Api key not registered";
            break;
        case TSDKErrorApiKeyRevoked:
            str = @"Api key revoked";
            break;
        case TSDKErrorLicenceDoesNotGrantFeature:
            str = @"Your license does not grant this feature";
            break;
        case TSDKErrorConnectionFailed:
            str = @"Connection Failed";
            break;
        case TSDKErrorServerResponse:
            str = @"Server response error";
            break;
        case TSDKErrorResourceNotFound:
            str = @"Resource not found";
            break;
        case TSDKErrorMissingLocationPermission:
            str = @"Missing location permission";
            break;
        case TSDKErrorMissingMotionFitnessPermission:
            str = @"Missing motion fitness permission";
            break;
        case TSDKErrorPermissionRefused:
            str = @"Permission refused";
            break;
        case TSDKErrorBluetoothFunctionUnavailable:
            str = @"Bluetooth function unavailable";
            break;
        case TSDKErrorAudioBlutoothUnavailable:
            str = @"Audio bluetooth unavailable";
            break;
        case TSDKErrorBluetoothDeviceOutOfRange:
            str = @"Bluetooth device out of range";
            break;
        case TSDKErrorBluetoothDeviceNotPaired:
            str = @"Bluetooth device not Paired";
            break;
        case TSDKErrorBluetoothAlreadyScanning:
            str = @"Bluetooth already scanning";
            break;
        case TSDKErrorGPSNotActivated:
            str = @"The GPS has not been activated";
            break;
        default:
            break;
    }
    return str;
}
@end
