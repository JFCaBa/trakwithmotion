//
//  NSError+TCoreExtension.h
//  TrakCoreFramework
//
//  Created by Jose Catala on 15/11/2018.
//  Copyright © 2018 TrakGlobal Telematics. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TrakCoreError.h"


NS_ASSUME_NONNULL_BEGIN

@interface NSError (TCoreExtension)

+ (NSString *) descriptionFromCode:(TSDKError)code;

+ (NSError *) errorWithCode:(TSDKError)code;

@end

NS_ASSUME_NONNULL_END
