//
//  TrakCoreJourneyEntity.m
//  TrakCoreFramework
//
//  Created by Jose Catala on 05/11/2018.
//  Copyright © 2018 TrakGlobal Telematics. All rights reserved.
//

#import "TrakCoreJourneyEntity.h"
#import "TrakCoreLocationEntity.h"
#import "TrakCoreDriverEvents.h"

#import <objc/runtime.h>

/********************
 *
 * TCoreRoutePoint
 *
 ********************/

@implementation TCoreRoutePoint

- (instancetype) initWithLocationEntity:(TCoreLocationEntity *)location
{
    self = [super init];
    
    if (self)
    {
        _latitude           = location.latitude;
        _longitude          = location.longitude;
        _speed              = location.speed;
        _horizontalAccuracy = location.horizontalAccuracy;
        _verticalAccuracy   = location.verticalAccuracy;
        _course             = location.course;
        _timestamp          = location.timestamp;
        _altitude           = location.altitude;
        _floor              = 0;
        _event              = TCorePointEventUnknown;
    }
    
    return self;
}

- (void)encodeWithCoder:(nonnull NSCoder *)aCoder
{
    [aCoder encodeFloat:self.latitude              forKey:@"latitude"];
    [aCoder encodeFloat:self.longitude             forKey:@"longitude"];
    [aCoder encodeFloat:self.speed                 forKey:@"speed"];
    [aCoder encodeFloat:self.horizontalAccuracy    forKey:@"horizontalAccuracy"];
    [aCoder encodeFloat:self.verticalAccuracy      forKey:@"verticalAccuracy"];
    [aCoder encodeFloat:self.course                forKey:@"course"];
    [aCoder encodeObject:self.timestamp            forKey:@"timestamp"];
    [aCoder encodeFloat:self.altitude              forKey:@"altitude"];
    [aCoder encodeInteger:self.floor               forKey:@"floor"];
    [aCoder encodeInteger:self.event               forKey:@"event"];
}

- (nullable instancetype)initWithCoder:(nonnull NSCoder *)aDecoder
{
    self = [super init];
    
    if (self)
    {
        _latitude               = [aDecoder decodeFloatForKey:@"latitude"];
        _longitude              = [aDecoder decodeFloatForKey:@"longitude"];
        _speed                  = [aDecoder decodeFloatForKey:@"speed"];
        _horizontalAccuracy     = [aDecoder decodeFloatForKey:@"horizontalAccuracy"];
        _verticalAccuracy       = [aDecoder decodeFloatForKey:@"verticalAccuracy"];
        _course                 = [aDecoder decodeFloatForKey:@"course"];
        _timestamp              = [aDecoder decodeObjectOfClass:[NSDate class] forKey:@"timestamp"];
        _altitude               = [aDecoder decodeFloatForKey:@"altitude"];
        _floor                  = [aDecoder decodeIntegerForKey:@"floor"];
        _event                  = [aDecoder decodeIntegerForKey:@"event"];
    }
    
    return self;
}

- (nonnull id)copyWithZone:(nullable NSZone *)zone
{
    TCoreRoutePoint *copy = [[[self class] allocWithZone:zone] init];
    
    if (copy)
    {
        copy.latitude               = self.latitude;
        copy.longitude              = self.longitude;
        copy.speed                  = self.speed;
        copy.horizontalAccuracy     = self.horizontalAccuracy;
        copy.verticalAccuracy       = self.verticalAccuracy ;
        copy.course                 = self.course;
        copy.timestamp              = self.timestamp;
        copy.altitude               = self.altitude;
        copy.floor                  = self.floor;
        copy.event                  = self.event;
    }
    
    return copy;
}

#pragma mark -  Public methods

+ (NSDictionary *) dictionaryRepresentation:(TCoreRoutePoint *)sender
{
    NSMutableDictionary *mDic = [[NSMutableDictionary alloc]init];
    
    unsigned int numIvars = 0;
    Ivar * ivars = class_copyIvarList([self class], &numIvars);
    
    for(int i = 0; i < numIvars; i++)
    {
        Ivar thisIvar = ivars[i];
        NSString * classKey = [NSString stringWithUTF8String:ivar_getName(thisIvar)];
        
        id obj = [sender valueForKey:classKey];
        
        //id value = [decoder decodeObjectForKey:key];
        if ([obj isKindOfClass:[NSNull class]])
        {
            obj = nil;
        }
        
        if (obj) [mDic setObject:obj forKey:classKey];
    }
    
    if (numIvars > 0)  free(ivars);
    
    return [mDic copy];
}

+ (TCoreRoutePoint *) pointRepresentation:(NSDictionary *)sender
{
    unsigned int numIvars = 0;
    TCoreRoutePoint *point = [[TCoreRoutePoint alloc]init];
    Ivar * ivars = class_copyIvarList([point class], &numIvars);
    
    for(int i = 0; i < numIvars; i++)
    {
        Ivar thisIvar = ivars[i];
        NSString * classKey = [NSString stringWithUTF8String:ivar_getName(thisIvar)];
        
        id obj = [sender valueForKey:classKey];
        
        //id value = [decoder decodeObjectForKey:key];
        if ([obj isKindOfClass:[NSNull class]])
        {
            obj = nil;
        }
        
        [point setValue:obj forKey:classKey];
    }
    
    if (numIvars > 0)  free(ivars);
    
    return point;
}

+ (NSArray *) arrayOfPointFromArrayOfModel:(NSArray *)sender
{
    __block NSMutableArray *mArray = [[NSMutableArray alloc]init];
    
    [sender enumerateObjectsUsingBlock:^(TCoreRoutePoint* obj, NSUInteger idx, BOOL * _Nonnull stop)
    {
        @autoreleasepool {
            NSDictionary *dic = [TCoreRoutePoint dictionaryRepresentation:obj];
            [mArray addObject:dic];
        }
    }];
    
    return [mArray copy];
}

+ (NSArray *) arrayOfPointFromDictionaryArray:(NSArray *)sender
{
    __block NSMutableArray *mArray = [[NSMutableArray alloc]init];
    
    [sender enumerateObjectsUsingBlock:^(NSDictionary* obj, NSUInteger idx, BOOL * _Nonnull stop)
    {
        @autoreleasepool {
            TCoreRoutePoint *point = [TCoreRoutePoint pointRepresentation:obj];
            [mArray addObject:point];
        }
    }];
    
    return [mArray copy];
}

#pragma mark -  OverWriting method

- (NSString *)description
{
    unsigned int numIvars = 0;
    Ivar * ivars = class_copyIvarList([self class], &numIvars);
    
    NSMutableString *mStr = [[NSMutableString alloc]init];
    
    for(int i = 0; i < numIvars; i++)
    {
        Ivar thisIvar = ivars[i];
        NSString * classKey = [NSString stringWithUTF8String:ivar_getName(thisIvar)];
        
        NSString *line = [NSString stringWithFormat:@"%@: %@\n",classKey,[self valueForKey:classKey]];
        
        [mStr appendString:line];
    }
    
    if (numIvars > 0)  free(ivars);
    
    [mStr appendString:@"\n"];
    return [mStr copy];
}

+ (BOOL)supportsSecureCoding {
    return YES;
}

#pragma mark -  Private methods

- (NSString *)eventFromEnum:(TCoreDriverPointEvent)event
{
    NSString *str = @"";
    
    switch (event)
    {
        case TCorePointEventOFF:
            str = @"OFF";
            break;
        case TCorePointEventON:
            str = @"ON";
            break;
        case TCorePointEventSLC:
            str = @"SLC";
            break;
        case TCorePointEventUnknown:
            str = @"Unknown";
            break;
            
        default:
            break;
    }
    
    return str;
}


@end

/********************
 *
 * TCoreJourney
 *
 ********************/

@implementation TCoreJourney


- (instancetype) init
{
    self = [super init];
    
    if (self)
    {
        _route = [[NSMutableArray alloc]init];
    }
    
    return self;
}

- (void)encodeWithCoder:(nonnull NSCoder *)aCoder
{
    [aCoder encodeObject:self.journeyId         forKey:@"journeyId"];
    [aCoder encodeObject:self.customerRef       forKey:@"customerRef"];
    [aCoder encodeObject:self.route             forKey:@"route"];

}

- (nullable instancetype)initWithCoder:(nonnull NSCoder *)aDecoder
{
    self = [super init];
    
    if (self)
    {
        NSSet *classes = [NSSet setWithObjects:[TCoreRoutePoint class],[NSArray class], [TCoreJourney class], nil];
        _journeyId      = [aDecoder decodeObjectOfClasses:classes forKey:@"journeyId"];
        _customerRef    = [aDecoder decodeObjectOfClasses:classes forKey:@"customerRef"];
        _route          = [aDecoder decodeObjectOfClasses:classes forKey:@"route"];
    }
    
    return self;
}


- (nonnull id)copyWithZone:(nullable NSZone *)zone
{
    TCoreJourney *copy = [[[self class] allocWithZone:zone] init];
    
    if (copy)
    {
        copy.journeyId      = self.journeyId;
        copy.customerRef    = self.customerRef;
        copy.route          = self.route;
    }
    
    return copy;
}

- (nonnull id)mutableCopyWithZone:(NSZone *)zone
{
    TCoreJourney *mutableCopy = [[[self class] allocWithZone:zone] init];
    mutableCopy.route = [self.route mutableCopy];
    return mutableCopy;
}

#pragma mark -  Public methods

+ (NSDictionary *) dictionaryRepresentation:(TCoreJourney *)sender
{
    NSMutableDictionary *mDic = [[NSMutableDictionary alloc]init];
    
    unsigned int numIvars = 0;
    Ivar * ivars = class_copyIvarList([self class], &numIvars);
    
    for(int i = 0; i < numIvars; i++)
    {
        Ivar thisIvar = ivars[i];
        NSString * classKey = [NSString stringWithUTF8String:ivar_getName(thisIvar)];
        
        id obj = [sender valueForKey:classKey];
        
        //id value = [decoder decodeObjectForKey:key];
        if ([obj isKindOfClass:[NSNull class]])
        {
            obj = nil;
        }
        else if ([obj isKindOfClass:[NSArray class]])
        {
            obj = [TCoreRoutePoint arrayOfPointFromArrayOfModel:obj];
        }
        
        if (obj) [mDic setObject:obj forKey:classKey];
    }
    
    if (numIvars > 0)  free(ivars);
    
    return [mDic copy];
}

+ (TCoreJourney *) journeyFromDictionary:(NSDictionary *)sender
{
    unsigned int numIvars = 0;
    TCoreJourney *journey = [[TCoreJourney alloc]init];
    Ivar * ivars = class_copyIvarList([journey class], &numIvars);
    
    for(int i = 0; i < numIvars; i++)
    {
        Ivar thisIvar = ivars[i];
        NSString * classKey = [NSString stringWithUTF8String:ivar_getName(thisIvar)];
        
        id obj = [sender valueForKey:classKey];
        
        //id value = [decoder decodeObjectForKey:key];
        if ([obj isKindOfClass:[NSNull class]])
        {
            obj = nil;
        }
        else if ([obj isKindOfClass:[NSArray class]])
        {
            obj = [TCoreRoutePoint arrayOfPointFromDictionaryArray:obj];
        }
        
        [journey setValue:obj forKey:classKey];
    }
    
    if (numIvars > 0)  free(ivars);
    
    return journey;
}

#pragma mark -  OverWriting method

- (NSString *)description
{
    unsigned int numIvars = 0;
    Ivar * ivars = class_copyIvarList([self class], &numIvars);
    
    NSMutableString *mStr = [[NSMutableString alloc]initWithString:[NSString stringWithFormat:@"\n\n*** %@ Description ***\n",NSStringFromClass([self class])]];
    
    for(int i = 0; i < numIvars; i++)
    {
        Ivar thisIvar = ivars[i];
        NSString * classKey = [NSString stringWithUTF8String:ivar_getName(thisIvar)];
        
        NSString *line = [NSString stringWithFormat:@"%@: %@\n",classKey,[self valueForKey:classKey]];
        
        [mStr appendString:line];
    }
    
    if (numIvars > 0)  free(ivars);
    
    [mStr appendString:@"\n"];
    return [mStr copy];
}

+ (BOOL)supportsSecureCoding {
    return YES;
}

@end
