//
//  TrakSDKError.h
//  traksdk-ios
//
//  Created by IndieSpring on 11/09/2018.
//  Copyright © 2018 IndieSpring. All rights reserved.
//


#ifndef TrakCoreError_h
#define TrakCoreError_h

FOUNDATION_EXPORT const NSErrorDomain TrakSDKErrorDomain;


/*!
 * @enum TSDKError
 *
 * @discussion The possible errors returned during TSDK transactions.
 */
typedef NS_ENUM(NSInteger, TSDKError)
{
    TSDKErrorUnknown                        = 0,
    TSDKErrorNotImplemented                 = 1,
    TSDKErrorDetectDeviceAlreadyActive      = 2,
    TSDKErrorInvalidArguments               = 3,
    TSDKErrorJourneyAlreadyStarted          = 129,
    TSDKErrorAutomaticStartConflict         = 130,
    TSDKErrorJourneyNotRecording            = 131,
    TSDKErrorJourneyNotFound                = 132,
    TSDKErrorUserIdentifierNotRegistered    = 65,
    TSDKErrorApiKeyNotRegistered            = 66,
    TSDKErrorApiKeyRevoked                  = 67,
    TSDKErrorLicenceDoesNotGrantFeature     = 33,
    TSDKErrorConnectionFailed               = 17,
    TSDKErrorServerResponse                 = 18,
    TSDKErrorResourceNotFound               = 19,
    TSDKErrorMissingLocationPermission      = 257,
    TSDKErrorMissingMotionFitnessPermission = 258,
    TSDKErrorPermissionRefused              = 259,
    TSDKErrorBluetoothFunctionUnavailable   = 513,
    TSDKErrorAudioBlutoothUnavailable       = 514,
    TSDKErrorBluetoothDeviceOutOfRange      = 1025,
    TSDKErrorBluetoothDeviceNotPaired       = 1026,
    TSDKErrorBluetoothAlreadyScanning       = 1027,
    TSDKErrorGPSNotActivated                = 1028

};

#endif
