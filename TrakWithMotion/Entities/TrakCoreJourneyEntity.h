//
//  TrakCoreJourneyEntity.h
//  TrakCoreFramework
//
//  Created by Jose Catala on 05/11/2018.
//  Copyright © 2018 TrakGlobal Telematics. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TrakCoreDriverEvents.h"

@class TCoreLocationEntity;

NS_ASSUME_NONNULL_BEGIN

@interface TCoreRoutePoint : NSObject <NSSecureCoding, NSCopying>

@property (nonatomic) float latitude;
@property (nonatomic) float longitude;
@property (nonatomic) float speed;
@property (nonatomic) float horizontalAccuracy;
@property (nonatomic) float verticalAccuracy;
@property (nonatomic) float course;
@property (nonatomic) float altitude;
@property (nonatomic, copy) NSDate *timestamp;
@property (nonatomic) NSInteger floor;
@property (nonatomic) TCoreDriverPointEvent event;
@property (class, readonly) BOOL supportsSecureCoding;

- (instancetype) initWithLocationEntity:(TCoreLocationEntity *)location;

@end

@interface TCoreJourney : NSObject <NSCopying, NSMutableCopying, NSSecureCoding>

@property (nonatomic, strong) NSNumber * _Nullable journeyId;
@property (nonatomic, copy) NSString *customerRef;
@property (nonatomic, strong) NSMutableArray <TCoreRoutePoint*> * _Nullable route;
@property (class, readonly) BOOL supportsSecureCoding;

+ (NSDictionary *) dictionaryRepresentation:(TCoreJourney *)sender;

+ (TCoreJourney *) journeyFromDictionary:(NSDictionary *)sender;

@end

NS_ASSUME_NONNULL_END
