//
//  TrakCoreLocationEntity.m
//  TrakCoreFramework
//
//  Created by Jose Catala on 02/11/2018.
//  Copyright © 2018 TrakGlobal Telematics. All rights reserved.
//

#import "TrakCoreLocationEntity.h"
#import "TrakCoreMotionStatus.h"
#import <CoreLocation/CoreLocation.h>
#import <objc/runtime.h>

@implementation TCoreLocationEntity

#pragma mark -  Initializers

- (instancetype) init
{
    self = [super init];
    
    if (self)
    {
        
    }
    
    return self;
}

- (instancetype) initWithLocation:(CLLocation * _Nullable)location
{
    if (!location) return nil;
    
    self = [super init];
    
    if (self)
    {
        _latitude           = location.coordinate.latitude;
        _longitude          = location.coordinate.longitude;
        _altitude           = location.altitude;
        _horizontalAccuracy = location.horizontalAccuracy;
        _verticalAccuracy   = location.verticalAccuracy;
        _speed              = location.speed;
        _course             = location.course;
        _timestamp          = location.timestamp;
        _status             = TCoreMotionUnknown;
    }
    
    return self;
}

- (void)encodeWithCoder:(nonnull NSCoder *)aCoder
{
    [aCoder encodeFloat:self.latitude              forKey:@"latitude"];
    [aCoder encodeFloat:self.longitude             forKey:@"longitude"];
    [aCoder encodeFloat:self.altitude              forKey:@"altitude"];
    [aCoder encodeFloat:self.horizontalAccuracy    forKey:@"horizontalAccuracy"];
    [aCoder encodeFloat:self.verticalAccuracy      forKey:@"verticalAccuracy"];
    [aCoder encodeFloat:self.speed                 forKey:@"speed"];
    [aCoder encodeFloat:self.course                forKey:@"course"];
    [aCoder encodeObject:self.timestamp            forKey:@"timestamp"];
    [aCoder encodeInteger:self.status              forKey:@"status"];
}

- (nullable instancetype)initWithCoder:(nonnull NSCoder *)aDecoder
{
    self = [super init];
    
    if (self)
    {
        _latitude           = [aDecoder decodeFloatForKey:@"latitude"];
        _longitude          = [aDecoder decodeFloatForKey:@"longitude"];
        _altitude           = [aDecoder decodeFloatForKey:@"altitude"];
        _horizontalAccuracy = [aDecoder decodeFloatForKey:@"horizontalAccuracy"];
        _verticalAccuracy   = [aDecoder decodeFloatForKey:@"verticalAccuracy"];
        _verticalAccuracy   = [aDecoder decodeFloatForKey:@"verticalAccuracy"];
        _speed              = [aDecoder decodeFloatForKey:@"speed"];
        _course             = [aDecoder decodeFloatForKey:@"course"];
        _timestamp          = [aDecoder decodeObjectForKey:@"timestamp"];
        _status             = [aDecoder decodeIntegerForKey:@"status"];
    }
    
    return self;
}

- (nonnull id)copyWithZone:(nullable NSZone *)zone
{
    TCoreLocationEntity *copy = [[[self class] allocWithZone:zone] init];
    
    if (copy)
    {
        copy.latitude           = self.latitude;
        copy.longitude          = self.longitude;
        copy.altitude           = self.altitude;
        copy.horizontalAccuracy = self.horizontalAccuracy;
        copy.verticalAccuracy   = self.verticalAccuracy ;
        copy.speed              = self.speed;
        copy.course             = self.course;
        copy.timestamp          = self.timestamp;
        copy.status             = self.status;
    }
    
    return copy;
}

#pragma mark -  OverWriting method

- (NSString *)description
{
    unsigned int numIvars = 0;
    Ivar * ivars = class_copyIvarList([self class], &numIvars);
    
    NSMutableString *mStr = [[NSMutableString alloc]initWithString:@""];
    
    for(int i = 0; i < numIvars; i++)
    {
        Ivar thisIvar = ivars[i];
        NSString * classKey = [NSString stringWithUTF8String:ivar_getName(thisIvar)];
        NSString *line = @"";
        if ([classKey isEqualToString:@"_status"])
        {
            line = [NSString stringWithFormat:@"%@: %@",classKey,[TCoreLocationEntity statusFromEnum:[[self valueForKey:classKey] integerValue]]];
        }
        else
        {
           line = [NSString stringWithFormat:@"%@: %@",classKey,[self valueForKey:classKey]];
        }
        
        
        [mStr appendString:line];
        [mStr appendString:@"\n"];
    }
    
    if (numIvars > 0)  free(ivars);
    
    [mStr appendString:@"\n\n"];
    return [mStr copy];
}

#pragma mark - Public methods

+ (NSString *)statusFromEnum:(NSInteger)status
{
    NSString *str = @"";
    
    switch (status)
    {
        case TCoreMotionCycling:
            str = @"Cycling";
            break;
        case TCoreMotionRunning:
            str = @"Running";
            break;
        case TCoreMotionWalking:
            str = @"Walking";
            break;
        case TCoreMotionDriving:
            str = @"Driving";
            break;
        case TCoreMotionUnknown:
            str = @"Unknown";
            break;
        case TCoreMotionStationary:
            str = @"Stationary";
            break;
        case TCoreMotionDrivingPaused:
            str = @"Paused";
            break;
            
        default:
            break;
    }
    
    return str;
}
@end
