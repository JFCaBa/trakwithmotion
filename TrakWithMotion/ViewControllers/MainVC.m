//
//  MainVC.m
//  TrakWithMotion
//
//  Created by Jose Catala on 22/11/2018.
//  Copyright © 2018 TrakGlobal Telematics. All rights reserved.
//

#import "MainVC.h"
//Controllers
#import "TrakCoreMotionController.h"
#import "TrakCoreLocationController.h"
#import "TrakCoreGCDTimer.h"
//Frameworks
#import <MapKit/MapKit.h>

#define TIMER_INTERVAL  1.0f //TO CHECK MOTION ACTIVITY


@interface MainVC () <TrakCoreLocationControllerDelegate, MKMapViewDelegate, TrakCoreGCDTimerDelegate>
//Outlets
@property (weak, nonatomic) IBOutlet UISwitch *switchAutoZoom;
@property (weak, nonatomic) IBOutlet UILabel                *lblStatus;
@property (weak, nonatomic) IBOutlet MKMapView              *myMap;
//Controllers
@property (nonatomic, strong) TrakCoreLocationController    *location;
@property (nonatomic, strong) TrakCoreMotionController      *motion;
@property (strong, nonatomic) TrakCoreGCDTimer *timer;
//Private
@property (nonatomic) TCoreMotionStatus drivingStatus;
@property (strong, nonatomic) MKPolyline *polyline;
@property (strong, nonatomic) NSMutableArray *route;
@end

@implementation MainVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UILongPressGestureRecognizer *longTap = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longpressToGetLocation:)];
    [self.myMap addGestureRecognizer:longTap];
    
    self.route           = [[NSMutableArray alloc]init];
    self.location        = [[TrakCoreLocationController alloc] init];
    self.motion          = [[TrakCoreMotionController alloc] init];
    self.timer           = [[TrakCoreGCDTimer alloc]initWithInterval: TIMER_INTERVAL];
    self.timer.delegate  = self;
    self.myMap.delegate  = self;

    [self.timer startTimer];
    
#ifdef DEBUG
    [self.location startUpdatingLocations];
#endif
    
    [_motion checkMotionActivity];
    
    __weak __typeof(self) weakSelf = self;
    
    [self.motion motionStatusHandler:^(TCoreMotionStatus status)
    {
        weakSelf.drivingStatus = status;
        weakSelf.lblStatus.text = [TCoreLocationEntity statusFromEnum:status];
     }];
    
    [self.location locationUpdatedHandler:^(TCoreLocationEntity * _Nonnull location)
    {
        location.status = weakSelf.drivingStatus;
        
        NSLog(@"%@",location);
        
        /*{*/
        TCoreLocationEntity *oldLocation;
        if (weakSelf.route.count > 0){oldLocation = [weakSelf.route lastObject];}
        else{ oldLocation = location; [weakSelf.route addObject:location];}
        /*}*/
        
        if (weakSelf.drivingStatus != TCoreMotionStationary
            && oldLocation.latitude != location.latitude
            && oldLocation.longitude != location.longitude
            && location.speed > 2)
        {
            if (![weakSelf.location isUpdatingLocations]) [weakSelf.location startUpdatingLocations];
            [weakSelf.route addObject:location];
            [weakSelf updateMap];
        }
        else
        {
#ifndef DEBUG
            [weakSelf.location startMonitoringForSignificantLocationChanges];
#endif
        }
     }];
}

#pragma mark -  Timer delegate

- (void) timerFired:(id)sender
{
    [_motion checkMotionActivity];
}


#pragma mark - Map stuff

- (MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id<MKOverlay>)overlay
{
    MKPolylineRenderer* lineView = [[MKPolylineRenderer alloc] initWithPolyline:self.polyline];
    lineView.strokeColor = [UIColor blueColor];
    lineView.lineWidth = 7;
    return lineView;
}

-(void)zoomToPolyLine:(MKMapView*)map polyline:(MKPolyline*)polyline animated:(BOOL)animated
{
    [map setVisibleMapRect:[polyline boundingMapRect] edgePadding:UIEdgeInsetsMake(30.0, 30.0, 30.0, 30.0) animated:animated];
}

- (void) updateMap
{
    int pointCount = (int)[_route count];
    MKMapPoint* pointArr = malloc(sizeof(MKMapPoint) * pointCount);
    
    int pointArrIndex = 0;  //it's simpler to keep a separate index for pointArr
    for (int idx = 0; idx < [_route count]; idx++)
    {
        @autoreleasepool
        {
            TCoreLocationEntity *entity = (TCoreLocationEntity *)[_route objectAtIndex:idx];
            CLLocationCoordinate2D workingCoordinate;
            workingCoordinate.latitude  = entity.latitude;
            workingCoordinate.longitude = entity.longitude;
            MKMapPoint point = MKMapPointForCoordinate(workingCoordinate);
            pointArr[pointArrIndex] = point;
            pointArrIndex++;
        }
    }
    
    // create the polyline based on the array of points.
    self.polyline = [MKPolyline polylineWithPoints:pointArr count:pointCount];
    [self.myMap addOverlay:_polyline level:MKOverlayLevelAboveRoads];
    free(pointArr);
    
    if ([_switchAutoZoom isOn]) {
        [self zoomToPolyLine:_myMap polyline:_polyline animated:YES];
    }
}

#pragma mark - Private

- (void)longpressToGetLocation:(UIGestureRecognizer *)gestureRecognizer
{
    if (gestureRecognizer.state != UIGestureRecognizerStateBegan)
        return;
    
    CGPoint touchPoint = [gestureRecognizer locationInView:self.myMap];
    CLLocationCoordinate2D location = [self.myMap convertPoint:touchPoint toCoordinateFromView:self.myMap];
    
    NSDictionary *dic = [self pointForCoordinates:location];
    
    if (!dic) return;
    
    TCoreRoutePoint *point = [dic objectForKey:@"point"];
    float travelled = [[dic objectForKey:@"travelled"] floatValue];
    
    [self showAlertWithTitle:@"Point" andMessage:[NSString stringWithFormat:@"%@Travelled:%0.2f",point,travelled]];
}

- ( NSDictionary * _Nullable )pointForCoordinates:(CLLocationCoordinate2D)coordinates
{
    __block TCoreLocationEntity *touchPoint = [[TCoreLocationEntity alloc]init];
    touchPoint.latitude = coordinates.latitude;
    touchPoint.longitude = coordinates.longitude;
    
    __block float minDistance = CGFLOAT_MAX;
    
    __block TCoreLocationEntity *point = nil;
    
    __block float travelled = 0;
    
    __weak __typeof(self) weakSelf = self;
    
    [_route enumerateObjectsUsingBlock:^(TCoreLocationEntity * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop)
    {
        float distance = [self distanceFrom:touchPoint and:obj];
        
        if (idx > 0)
        {
            float tdist = [self distanceFrom:obj and:weakSelf.route[idx -1]];
            travelled += tdist;
        }
        
        if (distance < minDistance)
        {
            minDistance = distance;
            point = obj;
        }
    }];
    
    NSDictionary *dic = nil;
    
    if (point) {
        dic = @{@"point":point,@"travelled":@(travelled)};
    }
    
    return dic;
}

- (void) showAlertWithTitle:(NSString *)title andMessage:(NSString *)msg
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:msg preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *dismiss = [UIAlertAction actionWithTitle:@"Dismiss" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    
    [alert addAction:dismiss];
    
    NSMutableParagraphStyle *paraStyle = [[NSMutableParagraphStyle alloc] init];
    paraStyle.alignment = NSTextAlignmentLeft;
    
    NSMutableAttributedString *atrStr = [[NSMutableAttributedString alloc] initWithString:[msg lowercaseString] attributes:@{NSParagraphStyleAttributeName:paraStyle,NSFontAttributeName:[UIFont systemFontOfSize:13.0]}];
    
    [alert setValue:atrStr forKey:@"attributedMessage"];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (float) distanceFrom:(TCoreLocationEntity *)start and:(TCoreLocationEntity*)end
{
    CLLocation *startLocation = [[CLLocation alloc]initWithLatitude:start.latitude longitude:start.longitude];
    CLLocation   *endLocation = [[CLLocation alloc]initWithLatitude:end.latitude longitude:end.longitude];
    float distance = [startLocation distanceFromLocation:endLocation];
    
    return distance;
}

@end
