//
//  MainVC.h
//  TrakWithMotion
//
//  Created by Jose Catala on 22/11/2018.
//  Copyright © 2018 TrakGlobal Telematics. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MainVC : UIViewController

@end

NS_ASSUME_NONNULL_END
