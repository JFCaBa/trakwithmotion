//
//  TrakCoreDriverEvents.h
//  TrakCoreFramework
//
//  Created by Jose Catala on 06/11/2018.
//  Copyright © 2018 TrakGlobal Telematics. All rights reserved.
//

#ifndef TrakCoreDriverEvents_h
#define TrakCoreDriverEvents_h

typedef NS_ENUM(NSInteger, TCoreDriverPointEvent)
{
    TCorePointEventSLC,
    TCorePointEventON,
    TCorePointEventMV,
    TcorePointEventXB, //Braking event
    TCorePointEventOFF,
    TCorePointEventUnknown
};

#endif /* TrakCoreDriverEvents_h */
