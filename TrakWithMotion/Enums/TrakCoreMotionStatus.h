//
//  TrakCoreMotionStatus.h
//  TrakCoreFramework
//
//  Created by Jose Catala on 02/11/2018.
//  Copyright © 2018 TrakGlobal Telematics. All rights reserved.
//

#ifndef TrakCoreMotionStatus_h
#define TrakCoreMotionStatus_h

typedef NS_ENUM(NSInteger, TCoreMotionStatus)
{
    TCoreMotionDriving,
    TCoreMotionDrivingPaused,
    TCoreMotionStationary,
    TCoreMotionWalking,
    TCoreMotionCycling,
    TCoreMotionRunning,
    TCoreMotionUnknown
};

#endif /* TrakCoreMotionStatus_h */
