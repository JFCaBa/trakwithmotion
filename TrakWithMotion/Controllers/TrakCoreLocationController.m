//
//  TrakCoreLocationController.m
//  TrakCoreFramework
//
//  Created by Jose Catala on 02/11/2018.
//  Copyright © 2018 TrakGlobal Telematics. All rights reserved.
//

#import "TrakCoreLocationController.h"
//Entities
#import "TrakCoreJourneyEntity.h"
//Extensions
#import "NSError+TCoreExtension.h"
//Frameworks
#import <CoreLocation/CoreLocation.h>


#define __WALKING

#define TIMER_INTERVAL          10.0f
#define MAX_LOCATION_HISTORY     5.0f
#define MAX_VALID_ACCURACY      75.0F
#define MAX_AGE_TO_GET_LOCATION  5.0f

@interface TrakCoreLocationController() <CLLocationManagerDelegate>

@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, strong) NSMutableArray *locationsHistory;

@end

@implementation TrakCoreLocationController

#pragma mark -  Initializers

- (instancetype)init
{
    self = [super init];
    
    if (self)
    {
        _locationManager = [[CLLocationManager alloc]init];
        _locationManager.delegate = self;
        _locationManager.pausesLocationUpdatesAutomatically = NO;
        _locationManager.allowsBackgroundLocationUpdates = YES;
        _locationManager.activityType    = CLActivityTypeAutomotiveNavigation;
        /// Start updating the location using AccuracyKilometer. Will change to Best when driving detected
        _locationManager.desiredAccuracy = kCLLocationAccuracyKilometer;

        [self requestAuthorization];
        
        [_locationManager stopMonitoringSignificantLocationChanges];
        [_locationManager startMonitoringSignificantLocationChanges];
        _isUpdatingLocations = YES;
        
        _locationsHistory = [[NSMutableArray alloc]init];
    }
    
    return self;
}

#pragma mark -  CLLocationManager Delegate

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations
{
    TCoreLocationEntity *entity = [self checkLocationWithLocation:[locations lastObject]];
    
    if (entity)
    {
        if ([_delegate respondsToSelector:@selector(didUpdateLocation:)])
        {
            __weak __typeof(self) weakSelf = self;
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [weakSelf.delegate didUpdateLocation:entity];
            });
        }
        
        if (_locationUpdatedHandler) self.locationUpdatedHandler(entity);
    }
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    if ([_delegate respondsToSelector:@selector(locationDidFailWithError:)])
    {
        __weak __typeof(self) weakSelf = self;

        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf.delegate locationDidFailWithError:error];
        });
    }
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    if (status == kCLAuthorizationStatusNotDetermined && [manager respondsToSelector:@selector(requestAlwaysAuthorization)])
    {
        [manager requestAlwaysAuthorization];
    }
}

- (void)locationManagerDidPauseLocationUpdates:(CLLocationManager *)manager
{

}

- (void)locationManagerDidResumeLocationUpdates:(CLLocationManager *)manager
{

}

#pragma mark -  Private methods

/*!
 *@brief Check if the authorization allow to work the GPS
 */
- (BOOL) checkGPSAuthorizationStatus
{
    if (![CLLocationManager locationServicesEnabled] || [CLLocationManager authorizationStatus] != kCLAuthorizationStatusAuthorizedAlways)
    {
        NSError *error = [NSError errorWithCode:TSDKErrorPermissionRefused];
        
        __weak __typeof(self) weakSelf = self;
        
        if (_delegate && [_delegate respondsToSelector:@selector(locationDidFailWithError:)])
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [weakSelf.delegate locationDidFailWithError:error];
            });
        }
        
        return NO;
    }
    
    return YES;
}

- (TCoreLocationEntity *) checkLocationWithLocation:(CLLocation *)location
{
    __block TCoreLocationEntity *entity = nil;
    
    if (location.horizontalAccuracy < MAX_VALID_ACCURACY)
    {
        entity = [[TCoreLocationEntity alloc] initWithLocation:location];
    }
    else
    {
        //Add the locations when dont pass the initial filter
        if (_locationsHistory.count > MAX_LOCATION_HISTORY) [self.locationsHistory removeObjectAtIndex:0];
        [_locationsHistory addObject:location];
        
        //Check if the time with a valid location is too long
        if (_locationsHistory.count > 0)
        {
            CLLocation *old = [_locationsHistory firstObject];
            NSTimeInterval age = [location.timestamp timeIntervalSinceDate:old.timestamp];
            if (age > MAX_AGE_TO_GET_LOCATION)
            {
                entity = [[TCoreLocationEntity alloc] initWithLocation:location];
            }
        }
    }
    
    return entity;

}



#pragma mark - Public methods

- (void) requestAuthorization
{
    BOOL GPSOk = [self checkGPSAuthorizationStatus];
    
    if (!GPSOk)
    {
        //The location is not activated
        NSError *error = [NSError errorWithCode:TSDKErrorMissingMotionFitnessPermission];
        
        if ([_delegate respondsToSelector:@selector(locationDidFailWithError:)])
        {
            [self.delegate locationDidFailWithError:error];
        }
    }
    /// Request Always Authorization
    if ([_locationManager respondsToSelector:@selector(requestAlwaysAuthorization)])
    {
        [self.locationManager requestAlwaysAuthorization];
    }
}

- (void) startUpdatingLocations
{
    _isUpdatingLocations = YES;
    [_locationManager stopUpdatingLocation];
    [_locationManager stopMonitoringSignificantLocationChanges];
    _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    /// Checking GPS use authorization status
    BOOL GPSOk = [self checkGPSAuthorizationStatus];
    
    if (GPSOk)
    {
        [_locationManager startUpdatingLocation];
    }
    else
    {
        NSError *error = [NSError errorWithCode:TSDKErrorGPSNotActivated];
        
        if (_delegate && [_delegate respondsToSelector:@selector(locationDidFailWithError:)])
        {
            [self.delegate locationDidFailWithError:error];
        }
    }
}

- (void) startMonitoringForSignificantLocationChanges
{
    _isUpdatingLocations = NO;
    [_locationManager stopUpdatingLocation];
    [_locationManager stopMonitoringSignificantLocationChanges];
    [_locationManager startMonitoringSignificantLocationChanges];
}

- (void) locationUpdatedHandler:(void (^)(TCoreLocationEntity* location))locationUpdatedHandler
{
    self.locationUpdatedHandler = locationUpdatedHandler;
}

- (void) requestLocation
{
    [_locationManager requestLocation];
}

- (float) distanceFrom:(TCoreRoutePoint *)start and:(TCoreRoutePoint*)end
{
    CLLocation *startLocation = [[CLLocation alloc]initWithLatitude:start.latitude longitude:start.longitude];
    CLLocation   *endLocation = [[CLLocation alloc]initWithLatitude:end.latitude longitude:end.longitude];
    float distance = [startLocation distanceFromLocation:endLocation];
    
    return distance;
}

- (TCoreRoutePoint *) interpolatePointFrom:(TCoreRoutePoint *)start to:(TCoreRoutePoint *)end
{
    TCoreRoutePoint * newPoint = [[TCoreRoutePoint alloc]init];
    
    newPoint.latitude           = (start.latitude + end.latitude) / 2;
    newPoint.longitude          = (start.longitude + end.longitude) / 2;
    newPoint.speed              = (start.speed + end.speed) / 2;
    newPoint.horizontalAccuracy = start.horizontalAccuracy;
    newPoint.verticalAccuracy   = start.verticalAccuracy;
    newPoint.course             = (start.course + end.course) /2;
    newPoint.timestamp          = end.timestamp;
    newPoint.event              = end.event;
    
    return newPoint;
}



@end
