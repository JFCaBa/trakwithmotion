//
//  TrakCoreLocationController.h
//  TrakCoreFramework
//
//  Created by Jose Catala on 02/11/2018.
//  Copyright © 2018 TrakGlobal Telematics. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

#ifndef TrakCoreLocationEntity_h
#import "TrakCoreLocationEntity.h"
#endif

@class TCoreRoutePoint;

typedef void (^LocationUpdatedBlock)(TCoreLocationEntity * _Nonnull);

NS_ASSUME_NONNULL_BEGIN

@protocol TrakCoreLocationControllerDelegate <NSObject>

@optional

- (void) didUpdateLocation:(TCoreLocationEntity *)location;

- (void) locationDidFailWithError:(NSError *)error;

- (void) didChangeLocationAuthorizationStatus:(CLAuthorizationStatus)status;

@end


@interface TrakCoreLocationController : NSObject

@property (weak, nonatomic) id<TrakCoreLocationControllerDelegate>delegate;

@property (nonatomic, copy) LocationUpdatedBlock locationUpdatedHandler;
@property (readonly) BOOL isAuthorized;
@property (readonly) BOOL isUpdatingLocations;

#pragma Public methods

- (void) startUpdatingLocations;

- (void) startMonitoringForSignificantLocationChanges;

- (void) locationUpdatedHandler:(void (^)(TCoreLocationEntity* location))locationUpdatedHandler;

- (void) requestAuthorization;

- (void) requestLocation;

- (float) distanceFrom:(TCoreRoutePoint *)start and:(TCoreRoutePoint*)end;

- (TCoreRoutePoint *) interpolatePointFrom:(TCoreRoutePoint *)start to:(TCoreRoutePoint *)end;

@end

NS_ASSUME_NONNULL_END
