//
//  TrakCoreMotionController.h
//  TrakCoreFramework
//
//  Created by Jose Catala on 02/11/2018.
//  Copyright © 2018 TrakGlobal Telematics. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TrakCoreMotionStatus.h"

typedef void (^MotionStatusBlock)(TCoreMotionStatus);

NS_ASSUME_NONNULL_BEGIN


@interface TrakCoreMotionController : NSObject

@property (nonatomic, copy) MotionStatusBlock motionStatusHandler;

@property (readonly) BOOL isAuthorized;

#pragma mark -  Public methods

- (void) motionStatusHandler:(void (^)(TCoreMotionStatus status))motionStatusHandler;

- (void) checkMotionActivity;

- (void) requestAuthorization;

@end

NS_ASSUME_NONNULL_END
