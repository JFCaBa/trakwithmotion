//
//  TrakCoreMotionController.m
//  TrakCoreFramework
//
//  Created by Jose Catala on 02/11/2018.
//  Copyright © 2018 TrakGlobal Telematics. All rights reserved.
//

#import "TrakCoreMotionController.h"
//Extensions
#import "NSError+TCoreExtension.h"
//Frameworks
#import <CoreMotion/CoreMotion.h>

#define MOTION_INTERVAL_LONG    1
#define STEPS_NUMBER_TO_WALK    10

@interface TrakCoreMotionController()

@property (strong, nonatomic) CMMotionManager *motionManager;
@property (strong, nonatomic) NSOperationQueue* queue;
@property BOOL isDriving;

@end

@implementation TrakCoreMotionController

#pragma mark -  Initializers

- (instancetype)init
{
    self = [super init];
    
    if (self)
    {
        _isAuthorized = YES;
        
        [self checkMotionActivity];
    }
    
    return self;
}

/*!
 *@brief This method uses the CoreMotion to check the activity in the last 30 seconds
 */
- (void) checkMotionActivity
{
    if([CMMotionActivityManager isActivityAvailable])
    {        
        __weak __typeof(self) weakSelf = self;
        
        CMMotionActivityManager *cm = [[CMMotionActivityManager alloc] init];
        
        NSDate *now = [NSDate date];
        
        NSDate *last30Sec = [now dateByAddingTimeInterval: (-MOTION_INTERVAL_LONG)];
        
        [cm queryActivityStartingFromDate:last30Sec toDate:now toQueue:[NSOperationQueue mainQueue] withHandler:^(NSArray *activities, NSError *error)
         {
             __strong __typeof(self) strongSelf = weakSelf;
             
             if (error)
             {
                 if(strongSelf.motionStatusHandler) strongSelf.motionStatusHandler(TCoreMotionUnknown);
             }
             else
             {
                 [activities enumerateObjectsUsingBlock:^(CMMotionActivity *a, NSUInteger idx, BOOL * _Nonnull stop) {
                     if (a.confidence == CMMotionActivityConfidenceHigh)
                     {
                         if (a.stationary)
                         {
                             if(strongSelf.motionStatusHandler) strongSelf.motionStatusHandler(TCoreMotionStationary);
                         }
                         else if (a.walking)
                         {
                             if(strongSelf.motionStatusHandler) strongSelf.motionStatusHandler(TCoreMotionWalking);
                         }
                         else if (a.running)
                         {
                             if(strongSelf.motionStatusHandler) strongSelf.motionStatusHandler(TCoreMotionRunning);
                         }
                         else if (a.cycling)
                         {
                             if(strongSelf.motionStatusHandler) strongSelf.motionStatusHandler(TCoreMotionCycling);
                         }
                         else if (a.unknown)
                         {
                             if(strongSelf.motionStatusHandler) strongSelf.motionStatusHandler(TCoreMotionUnknown);
                         }
                         else if (a.automotive)
                         {
                             if(strongSelf.motionStatusHandler) strongSelf.motionStatusHandler(TCoreMotionDriving);
                         }
                     }
                 }];
             }
         }];
    }
    else
    {
        _isAuthorized = NO;
        
        if(_motionStatusHandler) self.motionStatusHandler(TCoreMotionUnknown);
    }
}

#pragma mark -  Public methods

- (void) motionStatusHandler:(void (^)(TCoreMotionStatus status))motionStatusHandler
{
    self.motionStatusHandler = motionStatusHandler;
}

- (void) requestAuthorization
{
    // First time we call this method will show the authorization request to the user
    [self checkMotionActivity];
}

@end
